package com.training.microservice.frontend.service;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.training.microservice.frontend.dto.Product;

@FeignClient(name = "catalog",fallback = CatalogServiceFallback.class)
public interface CatalogService {
    
	@GetMapping("/api/product/")
    public Iterable<Product> dataProduct();

    @GetMapping("/api/host")
    Map<String, Object> backendInfo();
}



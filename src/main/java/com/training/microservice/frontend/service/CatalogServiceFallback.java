package com.training.microservice.frontend.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.training.microservice.frontend.dto.Product;

@Component
public class CatalogServiceFallback implements CatalogService {
    @Override
    public Iterable<Product> dataProduct() {
        return new ArrayList<>();
    }

    @Override
    public Map<String, Object> backendInfo() {
        Map<String, Object> defaultValue = new HashMap<>();
        defaultValue.put("host", "localhost");
        defaultValue.put("address", "127.0.0.1");
        defaultValue.put("port", 0);
        return defaultValue;
    }
}

